# Deploy to Minikube

## Prerequisites

- [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)
- [Helm 3](https://v3.helm.sh/)
- [Minikube](https://minikube.sigs.k8s.io/)
- [Docker](https://www.docker.com/)
- [Make](https://www.gnu.org/software/make/)
- [jq](https://stedolan.github.io/jq/)

## Deploy

```bash
minikube start --driver=docker --addons=ingress

make minikube-deploy

curl $(minikube ip)
```
